#include "basic_functions.h"

#include <QDebug>
#include <QDir>

bool copyDir(const QString &src, const QString &dst)
{
    QDir dir(src);

    if(!dir.exists())
    {
        qDebug() << "src dir doesn't exist:" << src;
        return false;
    }

    for(const auto& d : dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
    {
        QString dst_path = dst + QDir::separator() + d;

        if(!dir.mkpath(dst_path))
        {
            qDebug() << "cannot make path:" << dst_path;
            return false;
        }

        if(!copyDir(src + QDir::separator() + d, dst_path))
        {
            qDebug() << "cannot copy dir "
                     << src + QDir::separator() + d
                     << "to" << dst_path;
            return false;
        }
    }

    for(const auto f : dir.entryList(QDir::Files))
    {
        if(!QFile::copy(src + QDir::separator() + f, dst + QDir::separator() + f))
        {
            qDebug() << "cannot copy file"
                     << src + QDir::separator() + f
                     << "to"
                     << dst + QDir::separator() + f;
            return false;
        }
    }

    return true;
}

QHostAddress getLocalNetworkIntefaceBySubnet(const QHostAddress &remote)
{
    const auto allInterfaces = QNetworkInterface::allInterfaces();

    for(const auto& i : allInterfaces)
    {
        if(!(i.flags() & QNetworkInterface::IsUp))
        {
            continue;
        }

        for(const auto& a : i.addressEntries())
        {
            if(remote.isInSubnet(a.ip(), a.netmask().toIPv4Address()))
            {
                return a.ip();
            }
        }
    }

    return QHostAddress();
}

quint16 CalculateChecksum(quint16 *usBuf, int size)
{
    unsigned long usChksum = 0;
    while(size > 1)
    {
        usChksum += *usBuf++;
        size -= sizeof(quint16);
    }

    if(size)
    {
        usChksum += *(quint8*) usBuf;
    }

    usChksum = (usChksum >> 16) + (usChksum & 0xffff);
    usChksum += (usChksum >> 16);

    return quint8(~usChksum);
}

quint16 bit_reverse_word(quint16 value)
{
    const quint16 mask0 = 0x5555;
    const quint16 mask1 = 0x3333;
    const quint16 mask2 = 0x0F0F;
    const quint16 mask3 = 0x00FF;

    auto tmp = value;

    tmp = (((~mask0) & tmp) >> 1) | ((mask0 & tmp) << 1);
    tmp = (((~mask1) & tmp) >> 2) | ((mask1 & tmp) << 2);
    tmp = (((~mask2) & tmp) >> 4) | ((mask2 & tmp) << 4);
    tmp = (((~mask3) & tmp) >> 8) | ((mask3 & tmp) << 8);

    return tmp;
}

quint8 reverse(quint8 b)
{
    b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
    b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
    b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
    return b;
}

const QJsonObject point2json(const QPointF &point)
{
   return QJsonObject {
       {"x", point.x() },
       {"y", point.y() }
   };
}

const QPointF pointFromJson(const QJsonObject &json)
{
   return QPointF(json["x"].toDouble(), json["y"].toDouble());
}

const QJsonObject line2json(const QLineF &line)
{
   return QJsonObject {
       {"p1", point2json(line.p1()) },
       {"p2", point2json(line.p2()) },
   };
}

const QLineF lineFromJson(const QJsonObject &json)
{
   return QLineF(pointFromJson(json["p1"].toObject()),
           pointFromJson(json["p2"].toObject()));
}

const QJsonValue real2Json(const qreal &real)
{
   return qIsNaN(real) ? QJsonValue(QJsonValue::Undefined) : real;
}

qreal realFromJson(const QJsonValue &json)
{
   return json.toDouble(std::numeric_limits<qreal>::quiet_NaN());
}

uint qHash(const QVector3D &v) noexcept
{
   return qHash(v.x() + v.y() + v.z(), 0xa03f); // arbitrary value
}
