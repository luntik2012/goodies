#pragma once

#include <QHash>
#include <QHostAddress>
#include <QJsonObject>
#include <QLineF>
#include <QMetaEnum>
#include <QNetworkInterface>
#include <QPointF>
#include <QVector3D>

#include <limits>
#include <cstddef>

quint16 CalculateChecksum(quint16 *usBuf, int size);

quint16 bit_reverse_word(quint16 value);

quint8 reverse(quint8 b);


const QJsonObject point2json(const QPointF &point);

const QPointF pointFromJson(const QJsonObject &json);

const QJsonObject line2json(const QLineF &line);

const QLineF lineFromJson(const QJsonObject &json);

const QJsonValue real2Json(const qreal &real);

qreal realFromJson(const QJsonValue &json);

template<typename T>
static const QString enumToString(const T &value) {
    return QMetaEnum::fromType<T>().valueToKey(value);
}

template<typename T>
static const T stringToEnum(const QString &name) {
    return T(QMetaEnum::fromType<T>().keyToValue(name.toStdString().c_str()));
}

Q_DECL_CONST_FUNCTION uint qHash(const QVector3D &v) noexcept;

template <typename T>
static constexpr T min() {
    return std::numeric_limits<T>::min();
}

template <typename T>
static constexpr T max() {
    return std::numeric_limits<T>::max();
}

template <typename T>
static constexpr T lowest() {
    return std::numeric_limits<T>::lowest();
}


#define INI_ENUM_FROM_JSON(type, name, default) \
    m_ ## name( json.contains(#name) ? stringToEnum<type>(#name) : default)


template<typename T, typename retT, retT(T::*func)() const, Qt::SortOrder order>
Q_DECL_CONST_FUNCTION Q_DECL_CONSTEXPR  inline
bool comparator(const T &a, const T &b) Q_DECL_NOTHROW {
    if constexpr(order == Qt::AscendingOrder)
    {
        return ((&a)->*func)() < ((&b)->*func)();
    }
    else
    {
        return ((&a)->*func)() > ((&b)->*func)();
    }
}

bool copyDir(const QString &src, const QString &dst);

/*!
 * \brief getLocalNetworkIntefaceBySubnet - get local address from the same subnet as remote
 * \param remote - remote address
 * \return valid local address on success, QHostAddress() otherwise
 */
QHostAddress getLocalNetworkIntefaceBySubnet(const QHostAddress &remote);
