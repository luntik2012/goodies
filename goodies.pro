QT			-= gui
QT			+= core

TARGET		= goodies
TEMPLATE	= lib

DEFINES		+= GOODIES_LIBRARY
DEFINES		+= QT_DEPRECATED_WARNINGS
DEFINES		+= QT_DISABLE_DEPRECATED_BEFORE=0x050901
CONFIG		-= debug_and_release
CONFIG		+= c++11 c++14 c++17

HEADERS		+= $$PWD/*.h
SOURCES		+= $$PWD/*.cpp

unix {
    target.path = /usr/lib
    INSTALLS += target
}
