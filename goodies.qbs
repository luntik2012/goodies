import qbs 1.0

StaticLibrary {
    name: "goodies"
    files: [
        "*.cpp",
        "*.h"
    ]

    Depends { name: "cpp" }

    Depends {
        name: "Qt"
        submodules: [
            "concurrent",
            "core",
            "network",
            "qml",
            "widgets"
        ]
    }

    cpp.includePaths: ["."]
    cpp.cxxLanguageVersion: "c++17"

    Export {
        Depends { name: 'cpp' }
        Depends {
            name: "Qt"
            submodules: [
                "concurrent",
                "core",
                "network",
                "qml",
                "widgets"
            ]
        }
        cpp.includePaths: [product.sourceDirectory]
        cpp.cxxLanguageVersion: "c++17"
    }

    Group {
        qbs.install: true;
        fileTagsFilter: product.type;
    }
}

