#pragma once

#include <QMutex>
#include <QVariant>

#include "rfproperty.h"

#define INI_SINGLETON(type) IniSingleton<type>::get()

template<class T>
class IniSingleton
{
public:
    static T get() {
        static QMutex mtx;
        QMutexLocker l(&mtx);

        auto variant = GlobalSettings::instance()->value(T::staticMetaObject.className());
        qDebug() << "classname" << T::staticMetaObject.className();
        qDebug() << "VARIANT" << variant;

        return T(variant);
    }

    static void set(const T &value) {
        static QMutex mtx;
        QMutexLocker l(&mtx);

        GlobalSettings::instance()->setValue(T::staticMetaObject.className(),
                                             QVariant(value));
        GlobalSettings::instance()->sync();
    }

    static void cleanup() {
        static QMutex mtx;
        QMutexLocker l(&mtx);

        GlobalSettings::instance()->setValue(T::staticMetaObject.className(), QVariant());
        GlobalSettings::instance()->sync();
    }
};
