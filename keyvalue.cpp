#include "keyvalue.h"

KeyValue::KeyValue(const QVariant &key,
                   const QVariant &value,
                   QObject *parent)
    : RFObject(parent)
    , INIT_FIELD(key)
    , INIT_FIELD(value) {}
