#pragma once

#include "rfobject.h"

class Q_DECL_EXPORT KeyValue : public RFObject
{
    Q_OBJECT

    RF_PROPERTY(QVariant, key);
    RF_PROPERTY(QVariant, value);

public:
    explicit KeyValue(const QVariant &key = "",
                      const QVariant &value = "",
                      QObject *parent = nullptr);

    RF_OBJECT(RFObject, KeyValue)
    ASSIGN_OPERATOR(RFObject, KeyValue)
};

Q_DECLARE_METATYPE(KeyValue)
Q_DECLARE_METATYPE(QVector<KeyValue>)
