#pragma once

#include <QAbstractListModel>
#include <QApplication>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QMutex>
#include <QStandardPaths>
#include <QTextStream>

#include <iostream>

#include "rfobject.h"

#define FILENAME(fn) (strrchr(fn, '/') ? strrchr(fn, '/') + 1 : fn)
#define WRAP_ENUM(ext_enum, field) field = ext_enum::field

namespace goodies {

static QtMessageHandler defaultMessageHandler;

QString appLogFileName();
void myMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);


class LogMsg : public RFObject
{
    Q_OBJECT
    RF_OBJECT(RFObject, LogMsg)
    DEFAULT_CONSTRUCTOR(RFObject, LogMsg)
    DEFAULT_DESTRUCTOR(LogMsg);

public:
    enum MessageType {
        QtDebugMsg = QtMsgType::QtDebugMsg,
        QtWarningMsg = QtMsgType::QtWarningMsg,
        QtCriticalMsg = QtMsgType::QtCriticalMsg,
        QtFatalMsg = QtMsgType::QtFatalMsg,
        QtInfoMsg = QtMsgType::QtInfoMsg,
        QtSystemMsg = QtMsgType::QtSystemMsg
    };
    Q_ENUM(MessageType)

    RF_PROPERTY(QDateTime, dateTime);
    RF_PROPERTY(MessageType, messageType);
    RF_PROPERTY(QString, message);

public:
    LogMsg(QDateTime dateTime,
           MessageType messageType,
           QString message,
           QObject* parent = nullptr);
};

class LogWorker : public QAbstractListModel
{
    Q_OBJECT

    INI_PROPERTY(bool, isLoggingEnabled, true);
    INI_PROPERTY(int, maxLinesCount, 10 * 1000);

public:
    enum LogWorkerRoles
    {
        Date = Qt::UserRole + 1,
        MsgType,
        Msg
    };
    Q_ENUM(LogWorkerRoles)

public:
    Q_INVOKABLE LogWorker(const QString filepath = appLogFileName());
    Q_INVOKABLE ~LogWorker();

public:
    Q_INVOKABLE QString filepath() const;
    static void initialize(QApplication &app);

private:
    friend void myMessageHandler(QtMsgType type,
                                 const QMessageLogContext &context,
                                 const QString &msg);

public:
    Q_INVOKABLE void writeMessage(QtMsgType type,
                                  const QMessageLogContext &context,
                                  const QString &msg);

private:
    QString m_filepath;
    QFile m_file;
    QTextStream m_fileStream;
    QMutex m_mtx;
    QVector<LogMsg> m_messages;

    // QAbstractListModel stuff
public:
    Q_INVOKABLE int rowCount(const QModelIndex& parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

protected:
    inline QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE { return m_roleNames; }

protected:
    QHash<int, QByteArray> m_roleNames = {
        {int(LogWorkerRoles::Date), "date"},
        {int(LogWorkerRoles::MsgType), "msg_type"},
        {int(LogWorkerRoles::Msg), "msg"}
    };

    // Q_REGISTER_METATYPE stuff
public:
    LogWorker(const LogWorker &other);
};

}

Q_DECLARE_METATYPE(goodies::LogWorker)
Q_DECLARE_METATYPE(goodies::LogMsg)
