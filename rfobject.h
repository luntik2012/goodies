#pragma once

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QObject>
#include <QMetaProperty>

#include "rfproperty.h"

#define JSON_CONSTRUCTOR(base_class, derived_class) \
    public: \
    Q_INVOKABLE derived_class(const QJsonObject &json, QObject *parent = nullptr) \
    : base_class(json, parent) \
{\
    constructFromJson(json); \
}

#define JSON_VALUE_CONSTRUCTOR(base_class, derived_class) \
    public: \
    Q_INVOKABLE derived_class(const QJsonValue &json, QObject *parent = nullptr) \
    : base_class(json.toObject(), parent) \
{\
    constructFromJson(json.toObject()); \
}

#define JSON_ASSING_OPERATOR(base_class, derived_class) \
    public: \
    Q_INVOKABLE derived_class& operator =(const QJsonObject &json) {\
    constructFromJson(json); \
    return *this; \
    }

#define JSON_VALUE_ASSING_OPERATOR(base_class, derived_class) \
    public: \
    Q_INVOKABLE derived_class& operator =(const QJsonValue &json) {\
    *this = json.toObject(); \
    return *this; \
    }

#define COPY_CONSTRUCTOR(base_class, derived_class) \
    public: \
    Q_INVOKABLE derived_class(const derived_class &other, QObject *parent = nullptr) \
    : base_class(other, parent) \
{\
    *this = other; \
}

#define ASSIGN_OPERATOR(base_class, derived_class) \
    public: \
    derived_class& operator =(const derived_class &other) \
{\
    base_class::operator =(other); \
    return *this; \
}

#define COMPARE_OPERATOR(base_class, derived_class) \
    public: \
    bool operator==(const derived_class &other) const \
    {\
        if(!base_class::operator==(other)) \
            return false; \
        for(int i = metaObject()->propertyOffset(); i < metaObject()->propertyCount(); i++) \
        {\
            auto key = metaObject()->property(i).name(); \
            \
            if(property(key) != other.property(key)) \
            {\
                return false; \
            } \
        } \
        \
        return true; \
    }

#define JSON_VALUE_OPERATOR(base_class) \
    public: \
        operator const QJsonValue() const {\
		return (base_class::operator const QJsonObject()); \
        }

#define DEFAULT_DESTRUCTOR(classname) \
public: \
    Q_INVOKABLE ~classname() Q_DECL_OVERRIDE = default

#define DEFAULT_CONSTRUCTOR(base_class, derived_class) \
    public: \
        Q_INVOKABLE derived_class(QObject *parent = nullptr) \
    : base_class(parent) {}

#define RF_OBJECT(base_class, derived_class) \
    /*Q_OBJECT*/ \
    JSON_CONSTRUCTOR(base_class, derived_class) \
    JSON_VALUE_CONSTRUCTOR(base_class, derived_class) \
    COPY_CONSTRUCTOR(base_class, derived_class)

class RFObject : public QObject
{
    Q_OBJECT

    // constructors
public:
    RFObject(QObject *parent = nullptr);

    RFObject(const QJsonObject &json, QObject *parent = nullptr);
    RFObject(const QJsonValue &json, QObject *parent = nullptr);

    RFObject(const RFObject &other, QObject *parent = nullptr);

    // operators
public:
    virtual RFObject& operator =(const RFObject &other);
    virtual RFObject& operator =(const QJsonObject &json);
    virtual RFObject& operator =(const QJsonValue &json);
    virtual operator const QJsonObject() const;
    virtual operator const QJsonValue() const;
    virtual bool operator==(const RFObject &other) const;

protected:
    bool constructFromJson(const QJsonObject &json);
};

QDebug operator<<(QDebug d, const RFObject &o);
