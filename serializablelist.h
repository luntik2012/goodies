#pragma once

#include <QList>
#include <QVariant>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#define INIT_ARRAY_FIELD_FROM_JSON(field) \
    m_ ## field(json[#field].toArray())

template <typename T>
/*!
 * \brief The SerializableList class - (de)serializable (from)to json QList
 * \warning only use this class with basic types or basic RFObject-classes.
 * don't use pointers as T
 */
class SerializableList : public QList<T>
{
public:
    SerializableList() = default;
    SerializableList(const QJsonArray &json) : QList<T>() {
        *this = json;
    }
    SerializableList(const QList<T> &other) : QList<T>(other) {}
    SerializableList(const QJsonObject &json)
        : SerializableList(QJsonValue(json).toArray()) {}
    SerializableList(const QJsonValue &json)
        : SerializableList(json.toArray()) {}
    SerializableList(std::initializer_list<T> l) : QList<T>(l) {}

public:
    SerializableList& operator =(const QJsonArray &json) {
        for(const auto& item : json)
        {
            //            *this << item.toVariant().value<T>();
            *this << T(item);
        }

        return *this;
    }

    SerializableList& operator =(const QJsonObject &json) {
        return *this = QJsonValue(json);
    }

    SerializableList& operator =(const QJsonValue &json) {
        return *this = json.toArray();
    }

    virtual operator QJsonArray() const {
        QJsonArray array;

        for(const auto& item : *this)
        {
            array << item;
        }

        return array;
    }

    virtual operator QJsonValue() const {
        return QJsonValue(QJsonArray(*this));
    }

    virtual operator QJsonObject() const {
        return *this;
    }
};

template<typename T>
class SerializableVectorBasic : public QVector<T>
{
public:
    SerializableVectorBasic() = default;
    SerializableVectorBasic(const QJsonArray &json) : QVector<T>() {
        *this = json;
    }
    SerializableVectorBasic(const QVector<T> &other) : QVector<T>(other) {}
    SerializableVectorBasic(const QJsonObject &json)
        : SerializableVectorBasic(QJsonValue(json).toArray()) {}
    SerializableVectorBasic(const QJsonValue &json)
        : SerializableVectorBasic(json.toArray()) {}
    SerializableVectorBasic(std::initializer_list<T> l) : QVector<T>(l) {}

public:
    SerializableVectorBasic& operator =(const QJsonArray &json) {
        for(const auto& item : json)
        {
                        *this << item.toVariant().value<T>();
//            *this << T(item);
        }

        return *this;
    }

    SerializableVectorBasic& operator =(const QJsonObject &json) {
        return *this = QJsonValue(json);
    }

    SerializableVectorBasic& operator =(const QJsonValue &json) {
        return *this = json.toArray();
    }

    virtual operator QJsonArray() const {
        QJsonArray array;

        for(const auto& item : *this)
        {
//            auto v = QJsonValue::fromVariant(item);
//            array << QJsonValue(item);
            array << QJsonValue::fromVariant(item);
        }

        return array;
    }

    virtual operator QJsonValue() const {
        return QJsonValue(QJsonArray(*this));
    }

    virtual operator QJsonObject() const {
        return *this;
    }
};

template<typename T>
class SerializableVector : public QVector<T>
{
public:
    SerializableVector() = default;
    SerializableVector(const QJsonArray &json) : QVector<T>() {
        *this = json;
    }
    SerializableVector(const QVector<T> &other) : QVector<T>(other) {}
    SerializableVector(const QJsonObject &json)
        : SerializableVector(QJsonValue(json).toArray()) {}
    SerializableVector(const QJsonValue &json)
        : SerializableVector(json.toArray()) {}
    SerializableVector(const QVariant &variant)
        : SerializableVector(variant.toJsonArray()) {}
    SerializableVector(std::initializer_list<T> l) : QVector<T>(l) {}

public:
    SerializableVector& operator =(const QJsonArray &json) {
        for(const auto& item : json)
        {
//                        *this << item.toVariant().value<T>();
            *this << T(item);
        }

        return *this;
    }

    SerializableVector& operator =(const QJsonObject &json) {
        return *this = QJsonValue(json);
    }

    SerializableVector& operator =(const QJsonValue &json) {
        return *this = json.toArray();
    }

    virtual operator QJsonArray() const {
        QJsonArray array;

        for(const auto& item : *this)
        {
//            auto v = QJsonValue::fromVariant(item);
            array << item;
//            array << QJsonValue::fromVariant(item);
            qt_noop();
        }

        return array;
    }

    virtual operator QJsonValue() const {
        return QJsonValue(QJsonArray(*this));
    }

    virtual operator QJsonObject() const {
        return *this;
    }

    virtual operator QVariant() const {
        return QVariant(QJsonArray(*this));
    }
};
