#pragma once

#include <QAbstractTableModel>
#include <QMutex>
#include <QVector2D>

#include "rfproperty.h"

class SeriesModel : public QAbstractTableModel
{
    Q_OBJECT

    RF_PROPERTY(float, minX);
    RF_PROPERTY(float, maxX);
    RF_PROPERTY(float, minY);
    RF_PROPERTY(float, maxY);

public:
    Q_INVOKABLE SeriesModel(QObject *parent = nullptr);
    Q_INVOKABLE SeriesModel(const SeriesModel &other);
    Q_INVOKABLE ~SeriesModel() Q_DECL_OVERRIDE = default;

public:
    Q_INVOKABLE int rowCount(
            const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE int columnCount(
            const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    Q_INVOKABLE QVariant data(const QModelIndex &modelIndex,
                              int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;

public slots:
    void append(const QVector<QVector2D> points);
    void replace(const QVector<QVector2D> points);
    void clear();
    void recalcLimits();

protected:
    QVector<QVector2D> m_points;
    QMutex m_mtx;
};

Q_DECLARE_METATYPE(SeriesModel);
