#pragma once

#include <QMutex>
#include <QSharedPointer>

#define SINGLETON(type) Singleton<type>::get().data()

template<class T>
class Singleton
{
public:
    static QSharedPointer<T> get() {
        static QMutex mtx;
        QMutexLocker l(&mtx);

        if(!m_instance)
        {
            m_instance.reset(static_cast<T*>(T::staticMetaObject.newInstance())
                             // FIXME: deleteLater doesn't work. Crashes without it
                             , &T::deleteLater);
        }

        return m_instance;
    }
    static void cleanup() {
        static QMutex mtx;
        QMutexLocker l(&mtx);

        m_instance.clear();
    }

    typedef QSharedPointer<T> FactorySharedPointer;

private:
    static FactorySharedPointer m_instance;
};

template<class T>
typename Singleton<T>::FactorySharedPointer Singleton<T>::m_instance;
